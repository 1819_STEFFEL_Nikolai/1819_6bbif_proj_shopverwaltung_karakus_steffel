package at.spengergasse.steffel_karakus_project_6bbif;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SteffelKarakusProject6bbifApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(SteffelKarakusProject6bbifApplication.class, args);
    }

}
