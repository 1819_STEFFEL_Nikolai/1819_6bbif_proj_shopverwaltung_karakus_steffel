package at.spengergasse.steffel_karakus_project_6bbif;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SteffelKarakusProject6bbifApplicationTests
{

    @Test
    public void contextLoads()
    {
    }

}
