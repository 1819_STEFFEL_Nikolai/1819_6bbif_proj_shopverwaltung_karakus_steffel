# Java Project for 6BBIF in year 2019

## Purpose

This is a continuation of the project from the 6BBIF
- Web
- Springboot
- Lombok

## Prerequisites

- Java 11 
- current Maven (minimum 3.6.0)
- Lombok
- Springboot
- Web

## How to start

- 'clone'
- 'mvn clean package'

## Whom to ask

Mahmut KARAKUS (kar15445@spengergasse.at)
Nikolai STEFFEL (ste17952@spengergasse.at)